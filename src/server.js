const headers = require('./headers');
const express = require('express');
const app = express();
const port = process.env.PORT || 3001;


const handlebars = require("express-handlebars").create({defaultLayout:'main'});
app.disable('x-powered-by');
app.engine("handlebars",handlebars.engine);
app.set("view engine",'handlebars');
app.set('port',port);

app.listen(port, function(){
    console.log('Express started on http://localhost:' +
        app.get('port') + '; press Ctrl-C to terminate');
});
//-----------------------------------------------------------------------------
function flatten (arr,rez){
let keyNames =Object.keys(arr);
for(let key of keyNames ){
    let item = arr[key];
    if(Array.isArray(item) ||  typeof item === 'object'){
        rez= rez.concat(flatten(item,[]));
    }else {
        let obj ={};
        obj[key]=item;
        rez.push(obj);
        }
    }
   return rez;
}
//-------------------------------------------------------------------------

function clearStr(str,symbols){
    let index;
    str = str.trim();
    for(symbol of symbols){
        while((index = str.indexOf(symbol))>-1){ str =str.substr(0,index)+str.substr(index+1,str.length);}
    }
    return str.trim();
}
//---------------------------------------------------------------------------------------
  app.use("/*", function(req,res,next){
      headers.setHeaders(res);
      next();
  });
//----------------------------------------------------------------------------------------
app.post("/str",function(req,res){
    let post = [];
    req.on("data", function(chunk){post.push(chunk);});
    req.on("end",function () {
                try{
                    //Split Json string on nods and filtering nods that have 'Nm' key
                    let schemeArr = (post.join('').split(",").filter(elem=>elem.indexOf('"Nm":')>-1));
                    let filteredArr=[];
                    //check every node and cutting no needed parts that remained from json format
                    for(let item of schemeArr ){
                        let rezStr = item.substr(item.indexOf('Nm'),item.lastIndexOf('"'));
                        rezStr = clearStr(rezStr,[ '"','\\','\n','}','{',']','[' ]);
                        filteredArr.push(rezStr);
                    }
                    res.write(JSON.stringify(filteredArr));
                    res.end("_Done");
                }catch (e) {
                    res.end("Error "+e.message);
                    console.log(e);
                    return;
                }

    });
});
//--------------------------------------------------------------------------------------------
app.post("/obj",function(req,res){
    let post = [];
    req.on("data", function(chunk){post.push(chunk);});
    req.on("end",function () {
                try{
                    //Get requests json and turn it to an Object,
                    //Flattening the Object for easy filtering Nm nods
                    let flatArr = flatten(JSON.parse( post.join('')), []);
                    //filtering flattened Array with Nm filter
                   let  filteredArr = flatArr.filter((item) => item['Nm']);
                    res.write(JSON.stringify(filteredArr));
                    res.end("done");
                }catch (e) {
                    res.end("Error -"+e.message);console.log(e
                    )}
    });
});
//---------------------------------------------------------------------------------------------

 app.get("/*",function (req,res) {
     res.render('partials/home',{port:port,host:req.get('host')});
 });
app.post("/*",function (req,res) {
    res.render('partials/home',{port:port,host:req.get('host')});
});
